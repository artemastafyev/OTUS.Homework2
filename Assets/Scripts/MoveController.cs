using UnityEngine;

public class MoveController : MonoBehaviour
{
    private Vector3 _minPosition;
    private Vector3 _maxPosition;
    private float _speed;
    private Direction _direction;
    private bool _startFalling = false;
    private Rigidbody _rigidBody;

    [SerializeField] float MinDistance = 2f;
    [SerializeField] float MaxDistance = 5f;
    [SerializeField] float MinSpead = 2f;
    [SerializeField] float MaxSpead = 5f;

    // Start is called before the first frame update
    void Start()
    {
        SetMinPosition();
        SetMaxPosition();
        SetSpeed();
        _direction = Random.Range(0, 2) == 0 ? Direction.Up : Direction.Down;
        _rigidBody = GetComponent<Rigidbody>();
    }

    private void SetSpeed()
    {
        _speed = Random.Range(MinSpead, MaxSpead) / 10;
    }

    private void SetMaxPosition()
    {
        _maxPosition = Vector3.up * Random.Range(MinDistance, MaxDistance);
    }

    private void SetMinPosition()
    {
        _minPosition = Vector3.down * Random.Range(MinDistance, MaxDistance);
    }

    // Update is called once per frame
    void Update()
    {
        if (_startFalling == false)
        {
            if (transform.position.y >= _maxPosition.y)
            {
                _direction = Direction.Down;
                SetMaxPosition();
                SetSpeed();
            }

            if (transform.position.y <= _minPosition.y)
            {
                _direction = Direction.Up;
                SetMinPosition();
                SetSpeed();
            }

            if (transform.position.y <= _maxPosition.y && _direction == Direction.Up)
            {
                transform.position = Vector3.Lerp(transform.position, _maxPosition + transform.position, Time.deltaTime * _speed);
            }

            if (transform.position.y >= _minPosition.y && _direction == Direction.Down)
            {
                transform.position = Vector3.Lerp(transform.position, _minPosition + transform.position, Time.deltaTime * _speed);
            }
        }

        if (Input.GetKey(KeyCode.Space))
        {
            _startFalling = true;
        }
    }

    private void FixedUpdate()
    {
        if (_startFalling)
        {
            _rigidBody.mass = Random.Range(0.1f, 15f);
            _rigidBody.useGravity = true;
        }
    }
}

public enum Direction
{
    Up,
    Down
}
