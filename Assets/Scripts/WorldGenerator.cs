using System.Collections.Generic;
using UnityEngine;

public class WorldGenerator : MonoBehaviour
{
    public int size = 10;

    public GameObject GameObjectPrefab;
    public GameObject GameObjectsContainer;

    [SerializeField]
    private List<Material> materials;

    private List<GameObject> gameObjects = new List<GameObject>();

    void Start()
    {
        GenerateObjects();
    }

    [ContextMenu("������� �������")]
    private void GenerateObjects()
    {
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                Vector3 position = Vector3.zero + Vector3.left * i + Vector3.forward * j;
                GameObject gameObject = Instantiate(GameObjectPrefab, position, Quaternion.identity, GameObjectsContainer.transform);
                gameObject.GetComponent<Renderer>().material = GetMaterial();
                gameObjects.Add(gameObject);
            }
        }
    }

    [ContextMenu("������� �������")]
    void RemoveObjects()
    {
        for (int i = 0; i < gameObjects.Count; i++)
        {
            DestroyImmediate(gameObjects[i]);
        }
    }

    private Material GetMaterial()
    {
        var index = Random.Range(0, materials.Count);
        return materials[index];
    }
}
